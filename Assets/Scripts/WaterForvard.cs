﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WaterForvard : LocalSingleton<WaterForvard>
{
    public Vector3 startPos = new Vector3(2.1f, -3f, 6.55f);
    public GameObject campos;
    public float camSpeed = 1f;
    public float speed = 1f;
    bool f;
    public bool finish = false;
    private void Update()
    {
        if (UIManager.Instance.tapToStart && !f)
        {
            f = true;
            transform.DOLocalMove(startPos, speed);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("FinishO"))
        {
            finish = true;
            transform.DOLocalMove(new Vector3(-5, -12, -35), 1).OnComplete(()=> 
            {
                PlayerTrigger.Instance.animation.Idle();
                UIManager.Instance.cameraPos.SetActive(false);
                PlayerTrigger.Instance.transform.DOLocalMove(new Vector3(-1f, 0, 0), .5f);
                PlayerTrigger.Instance.transform.DOLocalRotate(new Vector3(0, -180, 7f), .5f);
                campos.SetActive(true);
            });
            PlayerTrigger.Instance.FinishO();
        }
    }
}
