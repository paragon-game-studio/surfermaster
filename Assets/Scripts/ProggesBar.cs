﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProggesBar : MonoBehaviour
{
    [SerializeField] private Image proggresBar;
    [SerializeField] private GameObject proggres;
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject finishFlag;
    float fullDistance;
    void Start()
    {
        fullDistance = GetDistance();
    }
    private void FixedUpdate()
    {
        float newDistance = GetDistance();
        float progressValuve = Mathf.InverseLerp(fullDistance, 0, newDistance);
        UpdateProgressFill(progressValuve);

    }
    void UpdateProgressFill(float valuve)
    {
        proggresBar.fillAmount = valuve;


    }
    private float GetDistance()
    {
        return Vector3.Distance(player.transform.position, finishFlag.transform.position);
    }
}
