﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
public class UIManager : LocalSingleton<UIManager>
{

    #region
    public GameObject perfectImage, goodImage, okImage, missImage;
    public Color perfectColor;
    public Color missColor;
    public Color memoryColor;
    public float scaleSpeed;
    public float dlay;

    public void PerfectImage()
    {
        perfectImage.transform.DOScale(new Vector3(1.6f, 1.6f, 1.6f), scaleSpeed / 2).SetDelay(dlay).SetEase(Ease.OutBack);
        perfectImage.GetComponent<Text>().DOColor(perfectColor, scaleSpeed / 2);
        Invoke("ImageScaleZero", .7f);
    }
    public void GoodImage()
    {
        goodImage.transform.DOScale(Vector3.one, scaleSpeed).SetEase(Ease.Linear);
        Invoke("ImageScaleZero", 1.2f);
    }
    public void OkImage()
    {
        okImage.transform.DOScale(Vector3.one, scaleSpeed).SetEase(Ease.Linear);
        Invoke("ImageScaleZero", 1.2f);
    }
    public void MissImage()
    {
        missImage.transform.DOScale(Vector3.one, scaleSpeed).SetEase(Ease.Linear);
        missImage.GetComponent<Text>().DOColor(missColor, scaleSpeed);
        Invoke("ImageScaleZero", 1.2f);
    }
    public void ImageScaleZero()
    {
        perfectImage.GetComponent<Text>().DOColor(memoryColor, scaleSpeed / 2);
        perfectImage.transform.DOScale(Vector3.zero, scaleSpeed).SetEase(Ease.InOutBack);
        goodImage.transform.DOScale(Vector3.zero, scaleSpeed).SetEase(Ease.Linear);
        okImage.transform.DOScale(Vector3.zero, scaleSpeed).SetEase(Ease.Linear);
        missImage.transform.DOScale(Vector3.zero, scaleSpeed).SetEase(Ease.Linear);
    }
    #endregion
    #region
    public bool tapToStart = false;
    public bool first = false;
    public GameObject startButton;
    public GameObject buttonUI;
    public GameObject finishUI;
    public GameObject proggesBar;
    public GameObject cameraPos;
    public GameObject cameraPos1;
    public GameObject tutorialHand;
    public Animations anim;
    public Text levelText;
    public int lvlIndex = 1;
    private void Awake()
    {
        levelText.text = currLevel.ToString();

    }
    private void FixedUpdate()
    {
        if (tapToStart&& !first)
        {
            first = true;
            startButton.SetActive(false);
            buttonUI.SetActive(true);
            proggesBar.SetActive(true);
            cameraPos.SetActive(true);
            cameraPos1.SetActive(false);
            anim.Slip();
        }
    }
    public void TapToStart()
    {
        tapToStart = true;
    }
    #endregion

    public GameObject finish;
    public GameObject fail;

    private static int _currlevel=1; 
    public static int currLevel
    {
        get
        {
            return PlayerPrefs.GetInt("currLevel", _currlevel);
        }
        set
        { 
            PlayerPrefs.SetInt("currLevel", value);
            _currlevel = value;
        }
    }
    public void LevelFinish()
    {
        finish.transform.DOScale(Vector3.one, .5f).SetEase(Ease.Linear).SetDelay(.5f).OnComplete(()=> 
        {
            buttonUI.SetActive(false);
            proggesBar.SetActive(false);
        });
        
    }
    public void NextButton()
    {
        currLevel++;
        
        if (currLevel >3)
        {
            SDKManager.maxLevelCount = 2;
            SceneManager.LoadScene("Levell" + (((currLevel - 1) % SDKManager.maxLevelCount) +1));
        }
        else
        {
            SceneManager.LoadScene("Level" + (((currLevel - 1) % SDKManager.maxLevelCount) + 1));
        }
    }
    public void LevelFail()
    {
        fail.transform.DOScale(Vector3.one, .5f).SetEase(Ease.Linear);
    }
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}

