﻿using System;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using GameAnalyticsSDK;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SDKManager : MonoBehaviour
{
    public static int maxLevelCount = 3;
    private void Awake()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            FB.ActivateApp();
        }

        
        GameAnalytics.Initialize();
         
        
    }
    
    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }


    private void Start()
    {
        SceneManager.LoadScene("Level" + (((UIManager.currLevel - 1) % maxLevelCount) + 1)); 
    }

   
    
    
    
}
