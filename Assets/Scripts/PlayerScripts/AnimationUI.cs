﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public abstract class AnimationUI : Animations
{
    [Header("buton sayısı")]
    [Space]
    [SerializeField] private GameObject[] button;
    

    public int[] buttonIndex;
    public int animationInvertal = 3;
    public List<Color> color;
    float a = 1;
    int lvl;
    #region
    private void Awake()
    {
        lvl = SceneManager.GetActiveScene().buildIndex;
        RandomButton();
        color.Add(button[0].gameObject.GetComponent<Image>().color);
        color.Add(button[1].gameObject.GetComponent<Image>().color);
        color.Add(button[2].gameObject.GetComponent<Image>().color);

        color.Add(button[0].gameObject.GetComponent<Image>().color);
        color.Add(button[1].gameObject.GetComponent<Image>().color);
        color.Add(button[2].gameObject.GetComponent<Image>().color);




        color.Add(button[3].gameObject.GetComponent<Image>().color);
        color.Add(button[4].gameObject.GetComponent<Image>().color);
        color.Add(button[5].gameObject.GetComponent<Image>().color);

        color.Add(button[3].gameObject.GetComponent<Image>().color);
        color.Add(button[4].gameObject.GetComponent<Image>().color);
        color.Add(button[5].gameObject.GetComponent<Image>().color);



    }
    bool l = false;
    bool s = true;
    private void FixedUpdate()
    {
        if (animatonOnOff && !l)
        {
            l = true;
            s = false;
            ButtonFadeOn();
        }
        else if (!animatonOnOff && !s)
        {
            s = true;
            l = false;
            ButtonFadeOff();

        }

    }
    public void RandomButton()
    {
        //while (buttonIndex[0] == buttonIndex[1] || buttonIndex[1] == buttonIndex[2] || buttonIndex[0] == buttonIndex[2])
        //{
        //    buttonIndex[0] = Random.Range(0, animationInvertal);
        //    buttonIndex[1] = Random.Range(0, animationInvertal);
        //    buttonIndex[2] = Random.Range(0, animationInvertal);
        //}
        print(lvl);
        if (lvl == 1)
        {
            Button1(0);
        }
        else if (lvl == 2)
        {
            Button2();
        }
        else if (lvl == 3)
        {
            Button3(0);
        }
        else
        {
            Button1();
            Button2();
            Button3();
        }






    }

    public void Button1(int x = -300)
    {
        button[0/*buttonIndex[0]*/].SetActive(true);
        button[0/*buttonIndex[0]*/].transform.localPosition = new Vector3(x, -850, 0);
    }
    public void Button2()
    {
        button[1/*buttonIndex[0]*/].SetActive(true);
        button[1/*buttonIndex[0]*/].transform.localPosition = new Vector3(0, -850, 0);
    }
    public void Button3(int x = 300)
    {
        button[2/*buttonIndex[0]*/].SetActive(true);
        button[2/*buttonIndex[0]*/].transform.localPosition = new Vector3(x, -850, 0);
    }
    public void ButtonFadeOn()
    {
        button[0].GetComponent<Button>().enabled = false;
        button[1].GetComponent<Button>().enabled = false;
        button[2].GetComponent<Button>().enabled = false;


        color[0] = new Color(color[0].r, color[0].g, color[0].b, .45f);
        color[1] = new Color(color[1].r, color[1].g, color[1].b, .45f);
        color[2] = new Color(color[2].r, color[2].g, color[2].b, .45f);

        button[0].GetComponent<Image>().DOColor(color[0], .2f);
        button[1].GetComponent<Image>().DOColor(color[1], .2f);
        button[2].GetComponent<Image>().DOColor(color[2], .2f);


        color[6] = new Color(color[6].r, color[6].g, color[6].b, .45f);
        color[7] = new Color(color[7].r, color[7].g, color[7].b, .45f);
        color[8] = new Color(color[8].r, color[8].g, color[8].b, .45f);

        button[3].GetComponent<Image>().DOColor(color[6], .2f);
        button[4].GetComponent<Image>().DOColor(color[7], .2f);
        button[5].GetComponent<Image>().DOColor(color[8], .2f);
    }
    public void ButtonFadeOff()
    {
        button[0].GetComponent<Button>().enabled = true;
        button[1].GetComponent<Button>().enabled = true;
        button[2].GetComponent<Button>().enabled = true;


        color[3] = new Color(color[0].r, color[0].g, color[0].b, 1f);
        color[4] = new Color(color[1].r, color[1].g, color[1].b, 1f);
        color[5] = new Color(color[2].r, color[2].g, color[2].b, 1f);

        button[0].GetComponent<Image>().DOColor(color[3], .2f);
        button[1].GetComponent<Image>().DOColor(color[4], .2f);
        button[2].GetComponent<Image>().DOColor(color[5], .2f);


        color[6] = new Color(color[6].r, color[6].g, color[6].b, 1f);
        color[7] = new Color(color[7].r, color[7].g, color[7].b, 1f);
        color[8] = new Color(color[8].r, color[8].g, color[8].b, 1f);

        button[3].GetComponent<Image>().DOColor(color[6], .2f);
        button[4].GetComponent<Image>().DOColor(color[7], .2f);
        button[5].GetComponent<Image>().DOColor(color[8], .2f);
    }
    #endregion
}
