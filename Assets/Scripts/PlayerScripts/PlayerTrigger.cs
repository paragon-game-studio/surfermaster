﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Animancer;
using DG.Tweening;

public class PlayerTrigger : LocalSingleton<PlayerTrigger>
{
    public Animations animation;
    [HideInInspector]
    public bool buttonClic = false;
    [HideInInspector]
    public bool ButtonClic = false;
    [HideInInspector]
    public string trigger = "Untagged";
    public PlayerParticle particle;
    [HideInInspector]
    public float time, t;
    [HideInInspector]
    public bool obstacleTF, triggerON;
 

    public Vector3 upVector, downVector;

    public float startSpeed = 1f;

    bool f = false;
    bool slow = false;

    private void FixedUpdate()
    {
        
        if (UIManager.Instance.tapToStart && !f)
        {
            f = true;
            transform.DOLocalMove(downVector, startSpeed);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!triggerON)
        {
            triggerON = true;
            t = Time.time;
        }
        if (other.CompareTag("Slow"))
        {
            slow = true;
            Time.timeScale = .05f;
            other.gameObject.GetComponent<Collider>().enabled = false;
            UIManager.Instance.tutorialHand.SetActive(true);
            Invoke("TimeScale", .3f);
        }

        if (!other.CompareTag("FinishO"))
        {
            Invoke("NotClick",.4f);
        }
        

    }
    void TimeScale() { Time.timeScale = 1f; UIManager.Instance.tutorialHand.SetActive(false); }
    private void OnTriggerStay(Collider other)
    {
        trigger = other.gameObject.tag;
        
        if (trigger == animation.animName && buttonClic)
        {
            buttonClic = false;
            ButtonClic = true;
            if (slow)
            {
                TimeScale();
            }
            trigger = "Untagged";
            animation.animName = "Untagged";
            
            time = Time.time;
            if (other.gameObject.layer == 9)
            {
                transform.DOLocalMove(upVector, .5f);
            }
            else if (other.gameObject.layer == 10)
            {
                particle.boardFX.Play();
                Invoke("FXFalse", 1);
            }
            TrueButton(other.gameObject);
        }
        else if (trigger != animation.animName && buttonClic)
        {
            print("girdi3");
            Invoke("FalseButton", .5f);
        }
        
    }

    void NotClick()
    {
        if (!ButtonClic)
        {
            FalseButton();
            UIManager.Instance.MissImage();
        }
    }
    void TrueButton(GameObject other)
    {
        float a = time - t;
        if (a >= 0 && a < .15f)
        {
            UIManager.Instance.PerfectImage();
            particle.perfectFX.Play();
        }
        else if (a > .15f && a < .2f)
        {
            UIManager.Instance.GoodImage();
        }
        else
        {
            UIManager.Instance.OkImage();
        }
        obstacleTF = true;
        transform.DOLocalMove(downVector, .2f).SetDelay(.5f).OnComplete(()=> { Invoke("WaterSea", .5f); });
        //other.gameObject.transform.DOMove(new Vector3(other.gameObject.transform.position.x, 50, other.gameObject.transform.position.z), 5).SetDelay(.5f);
        Destroy(other.gameObject, .3f);
        triggerON = false;
        a = 0;
    }
    void FalseButton()
    {
        obstacleTF = false;
        animation.Fail();
    }
    void FXFalse()
    {
        particle.boardFX.Stop();
        particle.waterSea.Play();
    }
    public void FinishO()
    {
        transform.DOLocalMove(new Vector3(-.2f, 0, -2.5f),1).OnComplete(()=> 
        {
            UIManager.Instance.LevelFinish();
        });
        
    }
}
[System.Serializable]
public class PlayerParticle
{
    public ParticleSystem perfectFX;
    public ParticleSystem diamondFX;
    public ParticleSystem boardFX;
    public ParticleSystem loseFX;
    public ParticleSystem waterSea;
    public GameObject particleFoe;
}
