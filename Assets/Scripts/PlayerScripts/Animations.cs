﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Animancer;
using DG.Tweening;

public abstract class Animations : LocalSingleton<Animations>
{
    [HideInInspector] public AnimancerComponent playerAnimancer;
    public GameObject playerGObj;

    [Header("Animations")] [Space] [SerializeField]
    private AnimationClip idle,
        swiming,
        swimingUp,
        swimingDown,
        down,
        down1,
        tumble,
        tumble1,
        tumble180,
        _tumble180,
        slip,
        surfing,
        fail;

    [SerializeField] private GameObject cube;
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject surfBoard;
    [SerializeField] private float speed = .1f;
    [SerializeField] private float timeswim;

    public bool failB = false;
    public bool isParent { get; private set; }
    [HideInInspector] public bool animatonOnOff;

    float time;

    public string animName = "Untagged";

    #region

    void PlayAnim(AnimationClip clip, float speed = 1f, float fade = 0.25f)
    {
        animatonOnOff = true;
        PlayerTrigger.Instance.particle.particleFoe.SetActive(false);
        if (!PlayerTrigger.Instance.triggerON && clip != slip)
        {
            PlayerTrigger.Instance.triggerON = true;
            PlayerTrigger.Instance.t = Time.time;
        }

        PlayerTrigger.Instance.buttonClic = true;

        var state = playerAnimancer.Play(clip, fade);
        state.Speed = speed;
        if (clip != idle && clip != swiming && clip != down && clip != down1 && clip != slip && !failB)
        {
            StartCoroutine(AnimParent());
        }
    }

    public void Idle()
    {
        PlayerTrigger.Instance.particle.particleFoe.SetActive(false);
        var state = playerAnimancer.Play(idle, .35f);
        state.Speed = 1f;
        surfBoard.transform.SetParent(playerGObj.transform);
        SurfBoardTransform(surfBoard, new Vector3(-1.5f, -190, 3.7f), new Vector3(0.099f, -0.01f, 0.111f));
    }

    public void Fail()
    {
        PlayerTrigger.Instance.particle.particleFoe.SetActive(true);
        failB = true;
        var state = playerAnimancer.Play(fail, .35f);
        state.Speed = 1f;
        surfBoard.transform.SetParent(cube.transform);

        SurfBoardTransform(surfBoard, new Vector3(-18.655f, -182.008f, 1.409f), new Vector3(1.29f, 8.04f, -0.56f));

        Invoke("Slip", 2f);
    }

    public void Swim()
    {
        animatonOnOff = true;
        var state = playerAnimancer.Play(swimingDown, .35f);
        state.Speed = 1.2f;
        if (!PlayerTrigger.Instance.triggerON)
        {
            PlayerTrigger.Instance.triggerON = true;
            PlayerTrigger.Instance.t = Time.time;
        }

        PlayerTrigger.Instance.buttonClic = true;
        animName = "Swim";
        surfBoard.transform.SetParent(cube.transform);
        playerGObj.transform.DOLocalRotate(new Vector3(10, 0, 0), .5f);
        SurfBoardTransform(surfBoard, new Vector3(-2f, 180, 0), new Vector3(0.35f, 8.3f, -.94f));
        Invoke("Swiming", .25f);

        //var state = playerAnimancer.Play(swiming, .7f);
        //state.Speed = 1.2f;
        //Invoke("SwimUP", timeswim);
    }

    public void Swiming()
    {
        var state = playerAnimancer.Play(swiming, .35f);
        state.Speed = 1.2f;
        Invoke("SwimDown", timeswim);
    }

    public void SwimDown()
    {
        var state = playerAnimancer.Play(swimingUp, .35f);
        state.Speed = 1.2f;
        Invoke("Slip", .35f);
    }

    public void Slip()
    {
        if (!WaterForvard.Instance.finish)
        {
            failB = false;
            PlayerTrigger.Instance.buttonClic = false;
            PlayerTrigger.Instance.ButtonClic = false;
            animatonOnOff = false;
            PlayerTrigger.Instance.particle.particleFoe.SetActive(true);
            surfBoard.transform.SetParent(player.transform);
            SurfBoardTransform(surfBoard, new Vector3(35, 120, -245), new Vector3(0.045f, 0.055f, -0.117f));
            playerGObj.transform.DOLocalRotate(new Vector3(10, 10, 0), .5f);

            var state = playerAnimancer.Play(slip, .7f);
            state.Speed = 1;
        }
    }

    void SurfBoardTransform(GameObject obj, Vector3 rotate, Vector3 move)
    {
        obj.transform.DOLocalRotate(rotate, speed/2);
        obj.transform.DOLocalMove(move, speed/2);
    }


    public void Tumble1()
    {
        time = 1.2f;
        animName = "Trumble-2";
        PlayAnim(tumble1, 1, .3f);
    }

    public void Tumble()
    {
        time = 1f;
        animName = "Trumble-1";
        PlayAnim(tumble);
    }

    public void Tumble180()
    {
        time = 1f;
        animName = "Trumble-180-1";
        PlayAnim(tumble180, 1.3f);
    }

    public void Tumble2180()
    {
        time = 1f;
        animName = "Trumble-180-2";
        PlayAnim(_tumble180, 1.3f);
    }

    public void Surfing()
    {
        surfBoard.transform.SetParent(player.transform);
        SurfBoardTransform(surfBoard, new Vector3(14f, -270f, 125f), new Vector3(0.3f, -0.02f, -0.1f));
        time = 1f;
        animName = "Surfing";
        PlayAnim(surfing, 1f);
    }

    IEnumerator AnimParent()
    {
        yield return new WaitForSeconds(time);
        Slip();
    }

    #endregion
}