﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Animancer;
using DG.Tweening;

public class RabdomPath : AnimationUI
{
    [Header("Previev")]
    [Space]
    public GameObject[] animPrevievs;

    [Header("PathCreator And Previev Transform")]
    [SerializeField] Previev[] previev;
    int random;
    private void Start()
    {
        Invoke("Prev", .1f);
    }
    void Prev()
    {
        //random = 0; //Random.Range(0, previev.Length);
        //previev[random].pathPrefab.SetActive(true);
        //Instantiate(animPrevievs[0/*buttonIndex[0]*/], previev[random].previevTransform[0/*buttonIndex[0]*/].transform.position, Quaternion.identity);
        //Instantiate(animPrevievs[1/*buttonIndex[0]*/], previev[random].previevTransform[1/*buttonIndex[0]*/].transform.position, Quaternion.identity);
        //Instantiate(animPrevievs[2/*buttonIndex[0]*/], previev[random].previevTransform[2/*buttonIndex[0]*/].transform.position, Quaternion.identity);
        //Instantiate(animPrevievs[buttonIndex[Random.Range(0,2)]], previev[random].previevTransform[4].transform.position, Quaternion.identity);
        playerAnimancer = GameObject.FindGameObjectWithTag("Player").GetComponent<AnimancerComponent>();
        Idle();
    }



}
[System.Serializable]
public class Previev
{
    [Header("Previev Transforms")]
    [Space]
    public Transform[] previevTransform;
    [Header("Previev Transforms")]
    [Space]
    public GameObject pathPrefab;
}
