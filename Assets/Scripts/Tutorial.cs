﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Tutorial : MonoBehaviour
{
    private void OnEnable()
    {
        StartCoroutine(TutorialHand());
    }
    IEnumerator TutorialHand()
    {
        while (true)
        {
            transform.DOScale(new Vector3(0.7f, 0.7f, 0.7f), .02f);
            yield return new WaitForSeconds(.02f);
            transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), .02f);
            yield return new WaitForSeconds(.02f);
        }
    }
}
